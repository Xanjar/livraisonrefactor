import java.util.List;

public class Livreur implements Runnable{
    private List<Commande> commandes;

    private SupprCommande supprCommande;

    public Livreur(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public Livreur(List<Commande> commandes, SupprCommande supprCommande) {
        this.commandes = commandes;
        this.supprCommande = supprCommande;
    }

    public int livrer(){
        int prix = 0;
        int size = commandes.size();
        for (int i = 0; i < size; i++) {
            prix += commandes.get(0).getPrix();
            commandes.remove(0);
        }
        return prix;
    }

    @Override
    public void run() {
        System.out.println("le thread livre");
        int size = commandes.size();
        while (commandes.size()!=0) {
            supprCommande.remove(commandes);
        }

    }

}
