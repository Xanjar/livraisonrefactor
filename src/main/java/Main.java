
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        System.out.println(5);
        Thread.sleep(1000);
        System.out.println(4);
        Thread.sleep(1000);
        System.out.println(3);
        Thread.sleep(1000);
        System.out.println(2);
        Thread.sleep(1000);
        System.out.println(1);
        Thread.sleep(1000);
        System.out.println("Début du programme");

        List<Commande> commandes = Collections.synchronizedList(new ArrayList<>());
        System.out.println("Preparation des commandes");
        Restaurant restaurant = new Restaurant(commandes,25000);
        Thread c1 = new Thread(new Cuisinier(restaurant));
        Thread c2 = new Thread(new Cuisinier(restaurant));
        Thread c3 = new Thread(new Cuisinier(restaurant));
        Thread c4 = new Thread(new Cuisinier(restaurant));
        c1.start();
        c2.start();
        c3.start();
        c4.start();
        c1.join();
        c2.join();
        c3.join();
        c4.join();
        System.out.println("Tri des commande par ordre de priorité");
        Collections.sort(commandes);
        System.out.println("Livraison");
        Integer prix = 0;
        SupprCommande  supprCommande = new SupprCommande();
        Thread l1 = new Thread(new Livreur(commandes, supprCommande));
        Thread l2 = new Thread(new Livreur(commandes, supprCommande));
        Thread l3 = new Thread(new Livreur(commandes, supprCommande));
        Thread l4 = new Thread(new Livreur(commandes, supprCommande));
        l1.start();
        l2.start();
        l3.start();
        l4.start();
        l1.join();
        l2.join();
        l3.join();
        l4.join();
        System.out.println("Argent récolté : "+supprCommande.getPrix());
        /*restaurant.genererCommande();
        System.out.println("Tri des commande par ordre de priorité");
        Collections.sort(commandes);
        System.out.println(commandes.get(0).getPriorite());
        System.out.println("Livraison des commandes");
        Livreur livreur = new Livreur(commandes);
        System.out.println("Argent récolté : "+livreur.livrer());*/

        System.out.println(5);
        Thread.sleep(1000);
        System.out.println(4);
        Thread.sleep(1000);
        System.out.println(3);
        Thread.sleep(1000);
        System.out.println(2);
        Thread.sleep(1000);
        System.out.println(1);
        Thread.sleep(1000);
        System.out.println("Fin du programme");
    }

}
