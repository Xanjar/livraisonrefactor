import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

public class Restaurant {
    private List<Commande> commandes;
    private int nb;

    public Restaurant(List<Commande> commandes) {
        this.commandes = commandes;
        nb=100000;
    }

    public Restaurant(List<Commande> commandes, int nb) {
        this.commandes = commandes;
        this.nb = nb;
    }

    public void genererCommande(){
        Random rand = new Random();
        for (int i = 0; i < nb; i++) {
            HashMap<Article,Integer> map = new HashMap<Article,Integer>();
            map.put(Article.BURGER,rand.nextInt(10 + 1));
            map.put(Article.FRITES,rand.nextInt(10 + 1));
            map.put(Article.PATES,rand.nextInt(10 + 1));
            map.put(Article.SUSHI,rand.nextInt(10 + 1));
            map.put(Article.PIZZA,rand.nextInt(10 + 1));
            map.put(Article.TACOS,rand.nextInt(10 + 1));
            map.put(Article.TIRAMISU,rand.nextInt(10 - 1));
            this.commandes.add(new Commande(map,rand.nextInt((10 - 1) + 1) + 1,rand.nextInt((50 - 10) + 1) + 10));
        }
    }

}
