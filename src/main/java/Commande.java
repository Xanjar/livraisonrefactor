import java.util.Comparator;
import java.util.Map;

public class Commande implements Comparable<Commande> {

    private Map<Article, Integer> articles;
    private int priorite;

    private int prix;

    public Commande(Map<Article, Integer> articles, int priorite, int prix) {
        this.articles = articles;
        this.priorite = priorite;
        this.prix = prix;
    }

    public int getPriorite() {
        return priorite;
    }

    public void setPriorite(int priorite) {
        this.priorite = priorite;
    }

    public Map<Article, Integer> getArticles() {
        return articles;
    }

    public void setArticles(Map<Article, Integer> articles) {
        this.articles = articles;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    @Override
    public int compareTo(Commande o) {
        return Integer.compare(this.getPriorite(),o.getPriorite());
    }
}
