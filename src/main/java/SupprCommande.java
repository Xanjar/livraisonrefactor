import java.util.List;

public class SupprCommande {
    int prix;

    public SupprCommande() {
        this.prix = 0;
    }

    public int getPrix() {
        return prix;
    }

    public synchronized void remove(List<Commande> commandes){

        if(commandes.size()!=0) {
            prix += commandes.get(0).getPrix();
            commandes.remove(0);
        }
    }
}
