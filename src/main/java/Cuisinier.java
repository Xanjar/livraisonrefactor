public class Cuisinier implements Runnable {
    private Restaurant restaurant;

    public Cuisinier(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public void run() {
        System.out.println("le thread prepare");
        restaurant.genererCommande();
    }
}
